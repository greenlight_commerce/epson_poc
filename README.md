# Epson

## Prerequisites

Add the following entries to /etc/hosts file.

    127.0.0.1 epson.local

## Installation

    ./gradlew install

## Build

    ./gradlew build

## Initialize

    ./gradlew initialize

## Deployment

    ./gradlew pack

## Running

### Run

    ./run.sh

### Debug

    ./run.sh debug

## Testing

    ./gradlew test

    ./gradlew testweb

## Versioning

The project uses the following versions scheme that is compatible with Hybris packaging standard.

    v01.02.03_4-g555555

The meaning is:

1. Major number
2. Minor number
3. Hotfix number
4. Patch indicator consisting of commit number after the latest tag, dash and first six digits of git commit hash.

### Releasing

To create a new version, create an annotated tag with the specified format.

    vXX.YY.ZZ

where X, Y, Z are numbers padded with one zero.

Tag the commit with the following command:

    git tag -am 'release tag' v01.02.03

Push the tags to origin

    git push --tags
