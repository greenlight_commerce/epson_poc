#!/bin/sh

PLATFORM=hybris/bin/platform

if [ ! -d "$PLATFORM" ]
then
    echo "Build project with './gradlew install', './gradlew build' first"
    exit 1
fi

(cd "$PLATFORM"; ./hybrisserver.sh "$@")
